# navigation

#### 介绍
基于 [WebStackPage.github.io](https://github.com/WebStackPage/WebStackPage.github.io) 带管理后台的导航页

#### 软件架构
PHP + SqlLite + H5


#### 安装教程
- 带PHP的WEB环境
- 访问 http://you_domain/manage


#### 使用说明

1.  添加分类
2.  添加链接
3.  右上角预览/生成静态页

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
